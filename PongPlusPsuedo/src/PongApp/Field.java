package PongApp;

import javax.swing.*;
import java.awt.image.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.*;

//Very Generic
public abstract class Field extends JPanel{
	private Field self;
	private BufferedImage BG; //Should be an ANINAMTION not a lame Background image
	private Racket Player1;
	
	public Field(int Width_px, int Height_px, Image Background) {
		super();
		this.setSize(Width_px,Height_px);
		this.setLayout(null);
		this.setVisible(true);
		self = this;

		BG = new BufferedImage(Width_px,Height_px,BufferedImage.TYPE_INT_ARGB);
		Graphics2D Brush = (Graphics2D)BG.getGraphics();
		
		if(Background==null) {
			for(int x=0;x<BG.getWidth();x++){for(int y=0;y<BG.getHeight();y++){BG.setRGB(x,y,0);}}
			System.out.println("DEBUG: failed to paint");
			return;
		}

		AffineTransform scaler = new AffineTransform();
		scaler.scale(Width_px/((double)Background.getWidth(null)), Height_px/((double)Background.getHeight(null)));
		while(!Brush.drawImage(Background,scaler,null)){}
	}
	
	public void setRacket(Racket newRacket){
		Player1=newRacket;
	}
	
	@Override
	public void paint(Graphics pen) {
		System.out.println("DEBUG: Paint the racket");
		Graphics2D P = (Graphics2D)pen;
		P.drawImage(BG, 0, 0, null);
		if(Player1!=null) {
			System.out.println("DEBUG: Player 1 valid");
			P.drawImage(Player1.selfIMG,(int)Player1.X, (int)Player1.Y, null);
		}
	}
}