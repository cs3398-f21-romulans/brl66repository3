package PongApp;

import javax.swing.*;
import java.awt.Toolkit;
import java.awt.image.*;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.*;

public class App extends JFrame{
	private App self;
	BufferedImage Buffer; Graphics2D BufBrush;
	Thread picturePainter;
	Ball b;
	public App(int Width_px, int Height_px) {
		super();
		this.setSize(Width_px,Height_px);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
		this.repaint();
		self=this;
		Buffer=new BufferedImage(Width_px,Height_px,BufferedImage.TYPE_INT_ARGB);
		BufBrush = (Graphics2D)Buffer.getGraphics();
		Toolkit t = Toolkit.getDefaultToolkit();
		
		Image FieldBG = t.getImage(this.getClass().getResource("PongBG.png"));
		while(FieldBG.getWidth(null)<0 || FieldBG.getHeight(null)<0){}

		RacketBallCourt PracticeRomulanField = new RacketBallCourt(Width_px,Height_px,FieldBG);
		this.add(PracticeRomulanField);
		
		Racket racket = new Racket(50,250);
		b = new Ball(50);
		racket.X = 50;
		racket.Y = (Width_px=racket.selfIMG.getHeight())/2;
		PracticeRomulanField.setRacket(racket);
		
		
			this.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e){}
			
			@Override
			public void keyReleased(KeyEvent e){}
			
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() ==KeyEvent.VK_DOWN) {
					racket.Y+=3;
				}else if(e.getKeyCode() == KeyEvent.VK_UP) {
					racket.Y-=3;
				}
				//self.repaint();
			}
		});

		picturePainter = new Thread(){
			@Override
			public void run() {
				while(true) {
					repaint();
					try {
						Thread.sleep(1000);
					}catch(Exception error) {
						error.printStackTrace();
						return;
					}
				}
			}
		};
		while(b.Img==null){}
		picturePainter.start();
	}
	
	@Override
	public void paint(Graphics pen) {
		Graphics2D P = (Graphics2D)pen;
		for(Component x : this.getComponents()) {
			x.repaint();
			
		}
		//Draw the NON-JSwing stuff as well
		P.drawImage(b.Img, (int)b.Xpos, (int)b.Ypos, null);
		
	}
}