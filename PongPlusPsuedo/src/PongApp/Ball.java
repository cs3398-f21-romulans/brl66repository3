package PongApp;

import java.awt.image.*;
import java.awt.Graphics2D;
import java.awt.Color;

//Should IMPLEMENT a "Drawing interface" to PROTECt fields and be private
public class Ball {
	public BufferedImage Img; //could be animated type
	
	public double D;
	public double Xpos, Ypos,
		   Xvel, Yvel;

	public Ball(double Radius_px) {
		D=2*Radius_px;
		Xpos=0;
		Ypos=0;
		Xvel=3;
		Yvel=0;
		Img=new BufferedImage((int)D,(int)D,BufferedImage.TYPE_INT_ARGB);
		Graphics2D brush = (Graphics2D)Img.getGraphics();
		brush.setColor(new Color(255,0,0));
		brush.fillOval(0,0,Img.getWidth(),Img.getWidth());
		
	}
}